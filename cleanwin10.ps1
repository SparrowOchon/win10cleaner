
# Tweaked Win10 Initial Setup Script
# Primary Authors: Network Silence 2018
# NOTE: MAKE SURE YOU READ THIS SCRIPT CAREFULLY BEFORE RUNNING IT + ADJUST COMMENTING AS APPROPRIATE
#       This script will reboot your machine when completed.
##########
[CmdletBinding()]
Param(
  [Parameter(Mandatory=$True,Position=1)]
    [string]$installType,
  [Parameter(Mandatory=$True,Position=2)]
    [string]$browser
)
# Ask for elevated permissions if required
If (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]"Administrator")) {
    Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`" $installType $browser" -Verb RunAs
    Exit
}
# Disable Telemetry
Write-Host "Disabling Telemetry..."
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows\DataCollection" -Name "AllowTelemetry" -Type DWord -Value 0

function force-mkdir($path) {
    If (!(Test-Path $path)) {
        #Write-Host "-- Creating full path to: " $path -ForegroundColor White -BackgroundColor DarkGreen
        New-Item -ItemType Directory -Force -Path $path
    }
}

Write-Host "Defuse Windows search settings"
Set-WindowsSearchSetting -EnableWebResultsSetting $false

Write-Host "Set general privacy options"
Set-ItemProperty "HKCU:\Control Panel\International\User Profile" "HttpAcceptLanguageOptOut" 1
force-mkdir "HKCU:\Printers\Defaults"
Set-ItemProperty "HKCU:\Printers\Defaults" "NetID" "{00000000-0000-0000-0000-000000000000}"
force-mkdir "HKCU:\SOFTWARE\Microsoft\Input\TIPC"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Input\TIPC" "Enabled" 0
force-mkdir "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AdvertisingInfo" "Enabled" 0
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost" "EnableWebContentEvaluation" 0

Write-Host "Disable synchronisation of settings"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\SettingSync" "BackupPolicy" 0x3c
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\SettingSync" "DeviceMetadataUploaded" 0
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\SettingSync" "PriorLogons" 1
$groups = @("Accessibility","AppSync","BrowserSettings","Credentials",
            "DesktopTheme","Language","PackageState","Personalization",
            "StartLayout","Windows")
ForEach ($group in $groups) {
    force-mkdir "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\SettingSync\Groups\$group"
    Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\SettingSync\Groups\$group" "Enabled" 0
}
Write-Host "Set privacy policy accepted state to 0"
force-mkdir "HKCU:\SOFTWARE\Microsoft\Personalization\Settings"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Personalization\Settings" "AcceptedPrivacyPolicy" 0


Write-Host "Do not scan contact informations"
force-mkdir "HKCU:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\InputPersonalization\TrainedDataStore" "HarvestContacts" 0

Write-Host "Inking and typing settings"
force-mkdir "HKCU:\SOFTWARE\Microsoft\InputPersonalization"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\InputPersonalization" "RestrictImplicitInkCollection" 1
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\InputPersonalization" "RestrictImplicitTextCollection" 1

Write-Host "Microsoft Edge settings"
force-mkdir "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main"
Set-ItemProperty "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\Main" "DoNotTrack" 1
force-mkdir "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\User\Default\SearchScopes"
Set-ItemProperty "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\User\Default\SearchScopes" "ShowSearchSuggestionsGlobal" 0
force-mkdir "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\FlipAhead"
Set-ItemProperty "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\FlipAhead" "FPEnabled" 0
force-mkdir "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter"
Set-ItemProperty "HKCU:\SOFTWARE\Classes\Local Settings\Software\Microsoft\Windows\CurrentVersion\AppContainer\Storage\microsoft.microsoftedge_8wekyb3d8bbwe\MicrosoftEdge\PhishingFilter" "EnabledV9" 0

Write-Host "Disable background access of default apps"
ForEach($key in (Get-ChildItem "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications")) {
    Set-ItemProperty ("HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\BackgroundAccessApplications\" + $key.PSChildName) "Disabled" 1
}
Write-Host "Denying device access"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\LooselyCoupled" "Type" "LooselyCoupled"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\LooselyCoupled" "Value" "Deny"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\LooselyCoupled" "InitialAppValue" "Unspecified"
ForEach ($key in (Get-ChildItem "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global")) {
    If ($key.PSChildName -EQ "LooselyCoupled") {
        continue
    }
    Set-ItemProperty ("HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\" + $key.PSChildName) "Type" "InterfaceClass"
    Set-ItemProperty ("HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\" + $key.PSChildName) "Value" "Deny"
    Set-ItemProperty ("HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\" + $key.PSChildName) "InitialAppValue" "Unspecified"
}
Write-Host "Apply MarkC's mouse acceleration fix"
Set-ItemProperty "HKCU:\Control Panel\Mouse" "MouseSensitivity" "10"
Set-ItemProperty "HKCU:\Control Panel\Mouse" "MouseSpeed" "0"
Set-ItemProperty "HKCU:\Control Panel\Mouse" "MouseThreshold1" "0"
Set-ItemProperty "HKCU:\Control Panel\Mouse" "MouseThreshold2" "0"
Set-ItemProperty "HKCU:\Control Panel\Mouse" "SmoothMouseXCurve" ([byte[]](0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xCC, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00,
0x80, 0x99, 0x19, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x66, 0x26, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x33, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00))
Set-ItemProperty "HKCU:\Control Panel\Mouse" "SmoothMouseYCurve" ([byte[]](0x00, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x38, 0x00, 0x00, 0x00, 0x00, 0x00,
0x00, 0x00, 0x70, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA8, 0x00, 0x00,
0x00, 0x00, 0x00, 0x00, 0x00, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00))


Write-Host "Disable mouse pointer hiding"
Set-ItemProperty "HKCU:\Control Panel\Desktop" "UserPreferencesMask" ([byte[]](0x9e,
0x1e, 0x06, 0x80, 0x10, 0x00, 0x00, 0x00))

 # Remove useless animations
Write-Host "Disable Animations"
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\VisualEffects" "VisualFXSetting" 2
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "ListviewAlphaSelect" 0
Set-ItemProperty -Path "HKCU:\Control Panel\Desktop\WindowMetrics" -Name "MinAnimate" -Type String -Value "0"
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "TaskbarAnimations" 0
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\DWM" "AlwaysHibernateThumbnails" 0
Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name "FontSmoothing" -Type String -Value "0"
Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name "CursorBlinkRate" -Type String -Value "-1"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Internet Explorer\Main" "DisableFirstRunCustomize" 1
Set-ItemProperty -Path "HKCU:\Control Panel\Desktop" -Name "MenuShowDelay" -Type String -Value "0"


 #Remove Gamer BS
Write-Host "Disable Game DVR and Game Bar"
force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\GameDVR"
Set-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows\GameDVR" "AllowgameDVR" 0


 #Bring back functional Audio Slider
Write-Host "Restoring old volume slider"
force-mkdir "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\MTCUVC"
Set-ItemProperty "HKLM:\Software\Microsoft\Windows NT\CurrentVersion\MTCUVC" "EnableMtcUvc" 0

 #Hide Useless Data
Write-Host "Setting folder view options"
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "HideDrivesWithNoMedia" 0
Set-ItemProperty "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "ShowSyncProviderNotifications" 0


 #Blocking telemetry using DNS
Write-Host "Setting default explorer view to This PC"
Set-ItemProperty "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" "LaunchTo" 1
Write-Host "Adding telemetry domains to hosts file"
$hosts_file = "$env:systemroot\System32\drivers\etc\hosts"
$domains = @(
    "a-0001.a-msedge.net", "a-0001.dc-msedge.net", "a-0002.a-msedge.net", "a-0003.a-msedge.net", "a-0004.a-msedge.net", "a-0005.a-msedge.net",
    "a-0006.a-msedge.net", "a-0007.a-msedge.net", "a-0008.a-msedge.net", "a-0009.a-msedge.net", "a-0010.a-msedge.net", "a-0011.a-msedge.net",
    "a-0012.a-msedge.net", "a1621.g.akamai.net", "a1856.g2.akamai.net", "a1961.g.akamai.net", "a248.e.akamai.net", "a978.i6g1.akamai.net", "a.ads1.msn.com",
    "a.ads2.msads.net", "a.ads2.msn.com", "ac3.msn.com", "activity.windows.com", "adnexus.net", "adnxs.com", "ads1.msads.net", "ads1.msn.com", "ads.msn.com",
    "aidps.atdmt.com", "aka-cdn-ns.adtech.de", "a-msedge.net", "any.edge.bing.com", "a.rad.msn.com", "array101-prod.do.dsp.mp.microsoft.com",
    "array102-prod.do.dsp.mp.microsoft.com", "array103-prod.do.dsp.mp.microsoft.com", "array104-prod.do.dsp.mp.microsoft.com", "b.ads1.msn.com",
    "b.ads2.msads.net", "bingads.microsoft.com", "bl3301-a.1drv.com", "bl3301-c.1drv.com", "bl3301-g.1drv.com", "bn1304-e.1drv.com", "bn1306-a.1drv.com",
    "bn1306-e.1drv.com", "bn1306-g.1drv.com", "bn2b-cor001.api.p001.1drv.com", "bn2b-cor003.api.p001.1drv.com", "bn3p-cor001.api.p001.1drv.com",
    "b.rad.msn.com", "bs.serving-sys.com", "by3301-a.1drv.com", "by3301-c.1drv.com", "by3301-e.1drv.com", "c-0001.dc-msedge.net",
    "cache.datamart.windows.com", "c.atdmt.com", "ca.telemetry.microsoft.com", "cdn.atdmt.com", "cds26.ams9.msecn.net", "cds459.lcy.llnw.net",
    "cds965.lon.llnw.net", "ch1-cor001.api.p001.1drv.com", "ch1-cor002.api.p001.1drv.com", "ch3301-c.1drv.com", "ch3301-e.1drv.com", "ch3301-g.1drv.com",
    "ch3302-c.1drv.com", "ch3302-e.1drv.com", "choice.microsoft.com", "choice.microsoft.com.nsatc.net", "c.msn.com", "compatexchange1.trafficmanager.net",
    "compatexchange.cloudapp.net", "corpext.msitadfs.glbdns2.microsoft.com", "corp.sts.microsoft.com", "cp101-prod.do.dsp.mp.microsoft.com",
    "cs1.wpc.v0cdn.net", "db3aqu.atdmt.com", "db3wns2011111.wns.windows.com", "db5sch101100835.wns.windows.com", "db5sch101100917.wns.windows.com",
    "db5sch101101231.wns.windows.com", "db5sch101101334.wns.windows.com", "db5sch101101511.wns.windows.com", "db5sch101101739.wns.windows.com",
    "db5sch101101813.wns.windows.com", "db5sch101101835.wns.windows.com", "db5sch101101914.wns.windows.com", "db5sch101101939.wns.windows.com",
    "db5sch101110114.wns.windows.com", "db5sch101110206.wns.windows.com", "db5sch101110325.wns.windows.com", "db5sch101110328.wns.windows.com",
    "db5sch101110403.wns.windows.com", "db5sch101110626.wns.windows.com", "db5sch101110634.wns.windows.com", "db5sch101110740.wns.windows.com",
    "db5sch101110810.wns.windows.com", "db5sch101110816.wns.windows.com", "db5sch101110821.wns.windows.com", "db5sch101110822.wns.windows.com",
    "db5sch101110828.wns.windows.com", "db5sch103082111.wns.windows.com", "db5sch103082406.wns.windows.com", "db5sch103082712.wns.windows.com",
    "db5sch103091617.wns.windows.com", "db5sch103091715.wns.windows.com", "db5sch103092209.wns.windows.com", "db5sch103101411.wns.windows.com",
    "db5sch103102112.wns.windows.com", "db5sch103102609.wns.windows.com", "db5wns1d.wns.windows.com", "db5.wns.windows.com",
    "db6sch102090510.wns.windows.com", "db6sch102090811.wns.windows.com", "db6sch102091008.wns.windows.com", "db6sch102091105.wns.windows.com",
    "db6sch102091412.wns.windows.com", "db6sch102091603.wns.windows.com", "dev.virtualearth.net", "df.telemetry.microsoft.com",
    "diagnostics.support.microsoft.com", "e2835.dspb.akamaiedge.net", "e7341.g.akamaiedge.net", "e7502.ce.akamaiedge.net", "e8218.ce.akamaiedge.net",
    "ec.atdmt.com", "ecn.dev.virtualearth.net", "eu.vortex.data.microsoft.com", "fe2.update.microsoft.com.akadns.net", "feedback.microsoft-hohm.com",
    "feedback.search.microsoft.com", "feedback.windows.com", "flex.msn.com", "geo-prod.do.dsp.mp.microsoft.com", "geover-prod.do.dsp.mp.microsoft.com",
    "g.msn.com", "h1.msn.com", "h2.msn.com", "hostedocsp.globalsign.com", "i1.services.social.microsoft.com", "i1.services.social.microsoft.com.nsatc.net",
    "inference.location.live.net", "ipv6.msftncsi.com", "ipv6.msftncsi.com.edgesuite.net", "i-sn2-cor001.api.p001.1drv.com", "i-sn2-cor002.api.p001.1drv.com",
    "kv101-prod.do.dsp.mp.microsoft.com", "lb1.www.ms.akadns.net", "live.rads.msn.com", "ls2web.redmond.corp.microsoft.com", "m.adnxs.com",
    "mobile.pipe.aria.microsoft.com", "msedge.net", "msntest.serving-sys.com", "nexus.officeapps.live.com", "nexusrules.officeapps.live.com",
    "oca.telemetry.microsoft.com", "oca.telemetry.microsoft.com.nsatc.net", "onesettings-bn2.metron.live.com.nsatc.net",
    "onesettings-cy2.metron.live.com.nsatc.net", "onesettings-db5.metron.live.com.nsatc.net", "onesettings-db5.metron.live.nsatc.net",
    "onesettings-hk2.metron.live.com.nsatc.net", "pre.footprintpredict.com", "preview.msn.com", "rad.live.com", "rad.msn.com", "redir.metaservices.microsoft.com",
    "reports.wes.df.telemetry.microsoft.com", "schemas.microsoft.akadns.net", "secure.adnxs.com", "secure.flashtalking.com",
    "services.wes.df.telemetry.microsoft.com", "settings.data.glbdns2.microsoft.com", "settings.data.microsoft.com", "settings-sandbox.data.microsoft.com",
    "settings-win.data.microsoft.com", "sls.update.microsoft.com.akadns.net", "sn3301-c.1drv.com", "sn3301-e.1drv.com", "sn3301-g.1drv.com",
    "spynet2.microsoft.com", "spynetalt.microsoft.com", "spyneteurope.microsoft.akadns.net", "sqm.df.telemetry.microsoft.com", "sqm.telemetry.microsoft.com",
    "sqm.telemetry.microsoft.com.nsatc.net", "ssw.live.com", "statsfe1.ws.microsoft.com", "statsfe2.update.microsoft.com.akadns.net", "statsfe2.ws.microsoft.com",
    "storecatalogrevocation.storequality.microsoft.com", "survey.watson.microsoft.com", "t0.ssl.ak.dynamic.tiles.virtualearth.net", "t0.ssl.ak.tiles.virtualearth.net",
    "telecommand.telemetry.microsoft.com", "telecommand.telemetry.microsoft.com.nsatc.net", "telemetry.appex.bing.net", "telemetry.appex.bing.net:443",
    "telemetry.microsoft.com", "telemetry.urs.microsoft.com", "tsfe.trafficshaping.dsp.mp.microsoft.com", "v10.vortex-win.data.metron.live.com.nsatc.net",
    "v10.vortex-win.data.microsoft.com", "version.hybrid.api.here.com", "view.atdmt.com", "vortex-bn2.metron.live.com.nsatc.net",
    "vortex-cy2.metron.live.com.nsatc.net", "vortex.data.glbdns2.microsoft.com", "vortex.data.metron.live.com.nsatc.net", "vortex.data.microsoft.com",
    "vortex-db5.metron.live.com.nsatc.net", "vortex-hk2.metron.live.com.nsatc.net", "vortex-sandbox.data.microsoft.com",
    "vortex-win.data.metron.live.com.nsatc.net", "vortex-win.data.microsoft.com", "watson.live.com", "watson.microsoft.com", "watson.ppe.telemetry.microsoft.com",
    "watson.telemetry.microsoft.com", "watson.telemetry.microsoft.com.nsatc.net", "web.vortex.data.microsoft.com", "wes.df.telemetry.microsoft.com",
    "win10.ipv6.microsoft.com", "www.msedge.net",

    #Spotify Ads removal
"0.0.0.0 pubads.g.doubleclick.net","0.0.0.0 securepubads.g.doubleclick.net","0.0.0.0 www.googletagservices.com","0.0.0.0 gads.pubmatic.com","0.0.0.0 ads.pubmatic.com","0.0.0.0 tpc.googlesyndication.com","0.0.0.0 pagead2.googlesyndication.com","0.0.0.0 googleads.g.doubleclick.net","","0.0.0.0 lb.usemaxserver.de","0.0.0.0 tracking.klickthru.com","0.0.0.0 gsmtop.net","0.0.0.0 click.buzzcity.net","0.0.0.0 ads.admoda.com","0.0.0.0 stats.pflexads.com","0.0.0.0 a.glcdn.co","0.0.0.0 wwww.adleads.com","0.0.0.0 ad.madvertise.de","0.0.0.0 apps.buzzcity.net","0.0.0.0 ads.mobgold.com","0.0.0.0 android.bcfads.com","0.0.0.0 show.buzzcity.net","0.0.0.0 api.analytics.omgpop.com","0.0.0.0 r.edge.inmobicdn.net","0.0.0.0 www.mmnetwork.mobi","0.0.0.0 img.ads.huntmad.com","0.0.0.0 creative1cdn.mobfox.com","0.0.0.0 admicro2.vcmedia.vn","0.0.0.0 s3.phluant.com","0.0.0.0 c.vrvm.com","0.0.0.0 go.vrvm.com","0.0.0.0 static.estebull.com","0.0.0.0 mobile.banzai.it","0.0.0.0 ads.xxxad.net","0.0.0.0 img.ads.mojiva.com","0.0.0.0 adcontent.saymedia.com","0.0.0.0 ftpcontent.worldnow.com","0.0.0.0 s0.2mdn.net","0.0.0.0 bigmobileads.com","0.0.0.0 banners.bigmobileads.com","0.0.0.0 images.mpression.net","0.0.0.0 images.millennialmedia.com","0.0.0.0 oasc04012.247realmedia.com","0.0.0.0 assets.cntdy.mobi","0.0.0.0 ad.leadboltapps.net","0.0.0.0 i.tapit.com","0.0.0.0 cdn1.crispadvertising.com","0.0.0.0 cdn2.crispadvertising.com","0.0.0.0 rs-staticart.ybcdn.net","0.0.0.0 img.ads.taptapnetworks.com","0.0.0.0 c753738.r38.cf2.rackcdn.com","0.0.0.0 edge.reporo.net","0.0.0.0 ads.n-ws.org","0.0.0.0 adultmoda.com","0.0.0.0 ads.smartdevicemedia.com","0.0.0.0 m.adsymptotic.com","0.0.0.0 api.yp.com","0.0.0.0 asotrack1.fluentmobile.com","0.0.0.0 android-sdk31.transpera.com","0.0.0.0 apps.mobilityware.com","0.0.0.0 ads.mobilityware.com","0.0.0.0 netdna.reporo.net","0.0.0.0 www.eltrafiko.com","0.0.0.0 cdn.trafficforce.com","0.0.0.0 gts-ads.twistbox.com","0.0.0.0 static.cdn.gtsmobi.com","0.0.0.0 ads.matomymobile.com","0.0.0.0 ads.adiquity.com","0.0.0.0 img.ads.mobilefuse.net","0.0.0.0 as.adfonic.net","0.0.0.0 media.mobpartner.mobi","0.0.0.0 cdn.us.goldspotmedia.com","0.0.0.0 ads2.mediaarmor.com","0.0.0.0 mobiledl.adobe.com","0.0.0.0 ads.flurry.com","0.0.0.0 gemini.yahoo.com","0.0.0.0 d3anogn3pbtk4v.cloudfront.net","0.0.0.0 d3oltyb66oj2v8.cloudfront.net","0.0.0.0 d2bgg7rjywcwsy.cloudfront.net","0.0.0.0 a.vserv.mobi","0.0.0.0 admin.vserv.mobi","0.0.0.0 c.vserv.mobi","0.0.0.0 ads.vserv.mobi","0.0.0.0 sf.vserv.mobi","0.0.0.0 hybl9bazbc35.pflexads.com","0.0.0.0 hhbekxxw5d9e.pflexads.com","0.0.0.0 www.pflexads.com","0.0.0.0 orencia.pflexads.com","0.0.0.0 atti.velti.com","0.0.0.0 ru.velti.com","0.0.0.0 mwc.velti.com","0.0.0.0 cdn.celtra.com","0.0.0.0 ads.celtra.com","0.0.0.0 cache-ssl.celtra.com","0.0.0.0 cache.celtra.com","0.0.0.0 track.celtra.com","0.0.0.0 wv.inner-active.mobi","0.0.0.0 cdn1.inner-active.mobi","0.0.0.0 m2m1.inner-active.mobi","0.0.0.0 bos-tapreq01.jumptap.com","0.0.0.0 bos-tapreq02.jumptap.com","0.0.0.0 bos-tapreq03.jumptap.com","0.0.0.0 bos-tapreq04.jumptap.com","0.0.0.0 bos-tapreq05.jumptap.com","0.0.0.0 bos-tapreq06.jumptap.com","0.0.0.0 bos-tapreq07.jumptap.com","0.0.0.0 bos-tapreq08.jumptap.com","0.0.0.0 bos-tapreq09.jumptap.com","0.0.0.0 bos-tapreq10.jumptap.com","0.0.0.0 bos-tapreq11.jumptap.com","0.0.0.0 bos-tapreq12.jumptap.com","0.0.0.0 bos-tapreq13.jumptap.com","0.0.0.0 bos-tapreq14.jumptap.com","0.0.0.0 bos-tapreq15.jumptap.com","0.0.0.0 bos-tapreq16.jumptap.com","0.0.0.0 bos-tapreq17.jumptap.com","0.0.0.0 bos-tapreq18.jumptap.com","0.0.0.0 bos-tapreq19.jumptap.com","0.0.0.0 bos-tapreq20.jumptap.com","0.0.0.0 web64.jumptap.com","0.0.0.0 web63.jumptap.com","0.0.0.0 web65.jumptap.com","0.0.0.0 bo.jumptap.com","0.0.0.0 i.jumptap.com","0.0.0.0 a.applovin.com","0.0.0.0 pdn.applovin.com","0.0.0.0 mobpartner.mobi","0.0.0.0 go.mobpartner.mobi","0.0.0.0 r.mobpartner.mobi","0.0.0.0 uk-ad2.adinfuse.com","0.0.0.0 adinfuse.com","0.0.0.0 go.adinfuse.com","0.0.0.0 ad1.adinfuse.com","0.0.0.0 ad2.adinfuse.com","0.0.0.0 sky.adinfuse.com","0.0.0.0 orange-fr.adinfuse.com","0.0.0.0 sky-connect.adinfuse.com","0.0.0.0 uk-go.adinfuse.com","0.0.0.0 orangeuk-mc.adinfuse.com","0.0.0.0 intouch.adinfuse.com","0.0.0.0 funnel0.adinfuse.com","0.0.0.0 cvt.mydas.mobi","0.0.0.0 lp.mydas.mobi","0.0.0.0 golds.lp.mydas.mobi","0.0.0.0 suo.lp.mydas.mobi","0.0.0.0 aio.lp.mydas.mobi","0.0.0.0 lp.mp.mydas.mobi","0.0.0.0 media.mydas.mobi","0.0.0.0 neptune.appads.com","0.0.0.0 neptune1.appads.com","0.0.0.0 neptune2.appads.com","0.0.0.0 neptune3.appads.com","0.0.0.0 saturn.appads.com","0.0.0.0 saturn1.appads.com","0.0.0.0 saturn2.appads.com","0.0.0.0 saturn3.appads.com","0.0.0.0 jupiter.appads.com","0.0.0.0 jupiter1.appads.com","0.0.0.0 jupiter2.appads.com","0.0.0.0 jupiter3.appads.com","0.0.0.0 req.appads.com","0.0.0.0 req1.appads.com","0.0.0.0 req2.appads.com","0.0.0.0 req3.appads.com","0.0.0.0 swappit.tapad.com","0.0.0.0 campaign-tapad.s3.amazonaws.com","0.0.0.0 adsrv1.tapad.com","0.0.0.0 ads1.mojiva.com","0.0.0.0 ads2.mojiva.com","0.0.0.0 ads3.mojiva.com","0.0.0.0 ads4.mojiva.com","0.0.0.0 ads5.mojiva.com","0.0.0.0 r.w.inmobi.com","0.0.0.0 c.w.inmobi.com","0.0.0.0 adtracker.inmobi.com","0.0.0.0 china.inmobi.com","0.0.0.0 japan.inmobi.com","0.0.0.0 mdn1.phluantmobile.net","0.0.0.0 mdn2.phluantmobile.net","0.0.0.0 mdn3.phluantmobile.net","0.0.0.0 soma.smaato.net","0.0.0.0 c29new.smaato.net","0.0.0.0 c01.smaato.net","0.0.0.0 c02.smaato.net","0.0.0.0 c03.smaato.net","0.0.0.0 c04.smaato.net","0.0.0.0 c05.smaato.net","0.0.0.0 c06.smaato.net","0.0.0.0 c07.smaato.net","0.0.0.0 c08.smaato.net","0.0.0.0 c09.smaato.net","0.0.0.0 c10.smaato.net","0.0.0.0 c11.smaato.net","0.0.0.0 c12.smaato.net","0.0.0.0 c13.smaato.net","0.0.0.0 c14.smaato.net","0.0.0.0 c15.smaato.net","0.0.0.0 c16.smaato.net","0.0.0.0 c17.smaato.net","0.0.0.0 c18.smaato.net","0.0.0.0 c19.smaato.net","0.0.0.0 c20.smaato.net","0.0.0.0 c21.smaato.net","0.0.0.0 c22.smaato.net","0.0.0.0 c23.smaato.net","0.0.0.0 c24.smaato.net","0.0.0.0 c25.smaato.net","0.0.0.0 c26.smaato.net","0.0.0.0 c27.smaato.net","0.0.0.0 c28.smaato.net","0.0.0.0 c29.smaato.net","0.0.0.0 c30.smaato.net","0.0.0.0 c31.smaato.net","0.0.0.0 c32.smaato.net","0.0.0.0 c33.smaato.net","0.0.0.0 c34.smaato.net","0.0.0.0 c35.smaato.net","0.0.0.0 c36.smaato.net","0.0.0.0 c37.smaato.net","0.0.0.0 c38.smaato.net","0.0.0.0 c39.smaato.net","0.0.0.0 c40.smaato.net","0.0.0.0 c41.smaato.net","0.0.0.0 c42.smaato.net","0.0.0.0 c43.smaato.net","0.0.0.0 c44.smaato.net","0.0.0.0 c45.smaato.net","0.0.0.0 c46.smaato.net","0.0.0.0 c47.smaato.net","0.0.0.0 c48.smaato.net","0.0.0.0 c49.smaato.net","0.0.0.0 c50.smaato.net","0.0.0.0 c51.smaato.net","0.0.0.0 c52.smaato.net","0.0.0.0 c53.smaato.net","0.0.0.0 c54.smaato.net","0.0.0.0 c55.smaato.net","0.0.0.0 c56.smaato.net","0.0.0.0 c57.smaato.net","0.0.0.0 c58.smaato.net","0.0.0.0 c59.smaato.net","0.0.0.0 c60.smaato.net","0.0.0.0 f03.smaato.net","0.0.0.0 f04.smaato.net","0.0.0.0 f05.smaato.net","0.0.0.0 f06.smaato.net","0.0.0.0 f07.smaato.net","0.0.0.0 f08.smaato.net","0.0.0.0 f09.smaato.net","0.0.0.0 f10.smaato.net","0.0.0.0 f11.smaato.net","0.0.0.0 f12.smaato.net","0.0.0.0 f13.smaato.net","0.0.0.0 f14.smaato.net","0.0.0.0 f15.smaato.net","0.0.0.0 f16.smaato.net","0.0.0.0 f17.smaato.net","0.0.0.0 f18.smaato.net","0.0.0.0 f19.smaato.net","0.0.0.0 f20.smaato.net","0.0.0.0 f21.smaato.net","0.0.0.0 f22.smaato.net","0.0.0.0 f23.smaato.net","0.0.0.0 f24.smaato.net","0.0.0.0 f25.smaato.net","0.0.0.0 f26.smaato.net","0.0.0.0 f27.smaato.net","0.0.0.0 f28.smaato.net","0.0.0.0 f29.smaato.net","0.0.0.0 f30.smaato.net","0.0.0.0 f31.smaato.net","0.0.0.0 f32.smaato.net","0.0.0.0 f33.smaato.net","0.0.0.0 f34.smaato.net","0.0.0.0 f35.smaato.net","0.0.0.0 f36.smaato.net","0.0.0.0 f37.smaato.net","0.0.0.0 f38.smaato.net","0.0.0.0 f39.smaato.net","0.0.0.0 f40.smaato.net","0.0.0.0 f41.smaato.net","0.0.0.0 f42.smaato.net","0.0.0.0 f43.smaato.net","0.0.0.0 f44.smaato.net","0.0.0.0 f45.smaato.net","0.0.0.0 f46.smaato.net","0.0.0.0 f47.smaato.net","0.0.0.0 f48.smaato.net","0.0.0.0 f49.smaato.net","0.0.0.0 f50.smaato.net","0.0.0.0 f51.smaato.net","0.0.0.0 f52.smaato.net","0.0.0.0 f53.smaato.net","0.0.0.0 f54.smaato.net","0.0.0.0 f55.smaato.net","0.0.0.0 f56.smaato.net","0.0.0.0 f57.smaato.net","0.0.0.0 f58.smaato.net","0.0.0.0 f59.smaato.net","0.0.0.0 f60.smaato.net","0.0.0.0 applift.com","0.0.0.0 a.medialytics.com","0.0.0.0 c.medialytics.com","0.0.0.0 cdn.creative.medialytics.com","0.0.0.0 p.medialytics.com","0.0.0.0 px.cdn.creative.medialytics.com","0.0.0.0 t.medialytics.com","0.0.0.0 st.a-link.co.kr","0.0.0.0 cdn.ajillionmax.com","0.0.0.0 dispatch.admixer.co.kr","0.0.0.0 ifc.inmobi.com","0.0.0.0 thinknear-hosted.thinknearhub.com","0.0.0.0 adimg3.search.naver.net","0.0.0.0 coconuts.boy.jp","0.0.0.0 iacpromotion.s3.amazonaws.com","0.0.0.0 plugin.2easydroid.com","0.0.0.0 aos.wall.youmi.net","0.0.0.0 au.youmi.net","0.0.0.0 img.adecorp.co.kr","0.0.0.0 us0.adlibr.com","0.0.0.0 ad.parrot.mable-inc.com","0.0.0.0 ad.leadboltmobile.net","0.0.0.0 events.foreseeresults.com","0.0.0.0 survey.foreseeresults.com","0.0.0.0 m.quantserve.com","0.0.0.0 met.adwhirl.com","0.0.0.0 html5adkit.plusmo.s3.amazonaws.com","0.0.0.0 inneractive-assets.s3.amazonaws.com","0.0.0.0 strikeadcdn.s3.amazonaws.com","0.0.0.0 aax-us-west.amazon-adsystem.com","0.0.0.0 ads2.greystripe.com","0.0.0.0 ads.mdotm.com","0.0.0.0 cdn.mdotm.com","0.0.0.0 eqx.smartadserver.com","0.0.0.0 itx5-publicidad.smartadserver.com","0.0.0.0 itx5.smartadserver.com","0.0.0.0 tcy.smartadserver.com","0.0.0.0 ww13.smartadserver.com","0.0.0.0 ww234.smartadserver.com","0.0.0.0 ww264.smartadserver.com","0.0.0.0 ww362.smartadserver.com","0.0.0.0 ww55.smartadserver.com","0.0.0.0 img.ads1.mocean.mobi","0.0.0.0 img.ads2.mocean.mobi","0.0.0.0 img.ads3.mocean.mobi","0.0.0.0 img.ads4.mocean.mobi","0.0.0.0 img.ads1.mojiva.com","0.0.0.0 img.ads2.mojiva.com","0.0.0.0 img.ads3.mojiva.com","0.0.0.0 img.ads4.mojiva.com","0.0.0.0 analytics.localytics.com","0.0.0.0 ads.adadapted.com","0.0.0.0 sto3.spotify.com","0.0.0.0 idsync-ext.rlcdn.com","0.0.0.0 xe-3-3-1-0.core1.fra2.de.m247.com","0.0.0.0 fra1.decixfra.fastly.net","0.0.0.0 vlan106.as03.zur1.ch.m247.com","0.0.0.0 xe-0-0-3-0.agg1.zur1.ch.m247.com","0.0.0.0 eth-50-4-0.core-agg3.buc.ro.m247.com","0.0.0.0 d3rt1990lpmkn.cloudfront.net","0.0.0.0 adserver-as.adtech.advertising.com","0.0.0.0 ads.bizhut.com","0.0.0.0 be.ads.justpremium.com","0.0.0.0 ads.54646.co","0.0.0.0 ads-static.saymedia.com","0.0.0.0 track.clearsender.com","0.0.0.0 1364.tm.zedo.com","0.0.0.0 star-z-mini.c10r.facebook.com","0.0.0.0 secure-gg.imrworldwide.com","0.0.0.0 imagesrv.adition.com","0.0.0.0 ad3.adfarm1.adition.com","0.0.0.0 a301.w62d.akamai.net","0.0.0.0 a297.c.akamai.net","0.0.0.0 adeventtracker.spotify.com","0.0.0.0 ads-akp.spotify.com","0.0.0.0 audio-ak.spotify.com","0.0.0.0 audio-akp.spotify.com","0.0.0.0 analytics.spotify.com","0.0.0.0 assets.spotify.com","0.0.0.0 audio-cf.spotify.com","0.0.0.0 audio-fa.scdn.co","0.0.0.0 audio-fa.spotify.com","0.0.0.0 audio-fab.spotify.com","0.0.0.0 audio-fac-spotify.com","0.0.0.0 audio-gc.scdn.co","0.0.0.0 audio-gc.spotify.com","0.0.0.0 crashdump.spotify.com","0.0.0.0 crashdump.ciqe-gslb.spotify.com","0.0.0.0 heads-ak.spotify.com","0.0.0.0 heads-akp.spotify.com","0.0.0.0 heads-cf.spotify.com","0.0.0.0 heads-fa.scdn.co","0.0.0.0 heads-fa.spotify.com","0.0.0.0 heads-fab.spotify.com","0.0.0.0 heads-fac.spotify.com","0.0.0.0 links.spotify.com","0.0.0.0 log.spotify.com","0.0.0.0 log2.spotify.com","0.0.0.0 mp3ad.scdn.co","0.0.0.0 partner-service.spotify.com","0.0.0.0 partner-service-testing.spotify.com","0.0.0.0 payment-callback.spotify.com","0.0.0.0 pci.spotify.com","0.0.0.0 pixel.spotify.com","0.0.0.0 upgrade.scdn.co","0.0.0.0 upgrade.spotify.com","0.0.0.0 video-fa.scdn.co","0.0.0.0 video-fa.cdn.spotify.com","0.0.0.0 video-fa-b.cdn.spotify.com","0.0.0.0 audio-ak-spotify-com.akamaized.net","0.0.0.0 audio-ake.spotify.com.edgesuite.net","0.0.0.0 audio-akp-spotify-com.akamaized.net","0.0.0.0 audio4-ak.spotify.com.edgesuite.net","0.0.0.0 audio4-akp.spotify.com.edgesuite.net","0.0.0.0 eip-onnet.audio-ak.spotify.com.akahost.net","0.0.0.0 eip-tata.audio-ak.spotify.com.akahost.net","0.0.0.0 eip-ntt.audio-ak.spotify.com.akahost.net","0.0.0.0 event.spotxchange.com","0.0.0.0 heads4-ak.spotify.com.edgesuite.net","0.0.0.0 heads4-akp.spotify.com.edgesuite.net","0.0.0.0 search.spotxchange.com","0.0.0.0 sync.search.spotxchange.com","0.0.0.0 spotxchange.com","0.0.0.0 www.spotx.tv","0.0.0.0 00.video-ak.cdn.spotify.com","0.0.0.0 01.video-ak.cdn.spotify.com","0.0.0.0 02.video-ak.cdn.spotify.com","0.0.0.0 03.video-ak.cdn.spotify.com","0.0.0.0 04.video-ak.cdn.spotify.com","0.0.0.0 05.video-ak.cdn.spotify.com","0.0.0.0 06.video-ak.cdn.spotify.com","0.0.0.0 07.video-ak.cdn.spotify.com","0.0.0.0 08.video-ak.cdn.spotify.com","0.0.0.0 09.video-ak.cdn.spotify.com","0.0.0.0 10.video-ak.cdn.spotify.com","0.0.0.0 11.video-ak.cdn.spotify.com","0.0.0.0 12.video-ak.cdn.spotify.com","0.0.0.0 13.video-ak.cdn.spotify.com","0.0.0.0 14.video-ak.cdn.spotify.com","0.0.0.0 15.video-ak.cdn.spotify.com","0.0.0.0 16.video-ak.cdn.spotify.com","0.0.0.0 17.video-ak.cdn.spotify.com","0.0.0.0 18.video-ak.cdn.spotify.com","0.0.0.0 19.video-ak.cdn.spotify.com","0.0.0.0 1a.video-ak.cdn.spotify.com","0.0.0.0 1b.video-ak.cdn.spotify.com","0.0.0.0 1c.video-ak.cdn.spotify.com","0.0.0.0 1d.video-ak.cdn.spotify.com","0.0.0.0 1e.video-ak.cdn.spotify.com","0.0.0.0 1f.video-ak.cdn.spotify.com","0.0.0.0 20.video-ak.cdn.spotify.com","0.0.0.0 21.video-ak.cdn.spotify.com","0.0.0.0 22.video-ak.cdn.spotify.com","0.0.0.0 23.video-ak.cdn.spotify.com","0.0.0.0 24.video-ak.cdn.spotify.com","0.0.0.0 25.video-ak.cdn.spotify.com","0.0.0.0 26.video-ak.cdn.spotify.com","0.0.0.0 27.video-ak.cdn.spotify.com","0.0.0.0 28.video-ak.cdn.spotify.com","0.0.0.0 29.video-ak.cdn.spotify.com","0.0.0.0 2a.video-ak.cdn.spotify.com","0.0.0.0 2b.video-ak.cdn.spotify.com","0.0.0.0 2c.video-ak.cdn.spotify.com","0.0.0.0 2d.video-ak.cdn.spotify.com","0.0.0.0 2e.video-ak.cdn.spotify.com","0.0.0.0 2f.video-ak.cdn.spotify.com","0.0.0.0 30.video-ak.cdn.spotify.com","0.0.0.0 31.video-ak.cdn.spotify.com","0.0.0.0 32.video-ak.cdn.spotify.com","0.0.0.0 33.video-ak.cdn.spotify.com","0.0.0.0 34.video-ak.cdn.spotify.com","0.0.0.0 35.video-ak.cdn.spotify.com","0.0.0.0 36.video-ak.cdn.spotify.com","0.0.0.0 37.video-ak.cdn.spotify.com","0.0.0.0 38.video-ak.cdn.spotify.com","0.0.0.0 39.video-ak.cdn.spotify.com","0.0.0.0 3a.video-ak.cdn.spotify.com","0.0.0.0 3b.video-ak.cdn.spotify.com","0.0.0.0 3c.video-ak.cdn.spotify.com","0.0.0.0 3d.video-ak.cdn.spotify.com","0.0.0.0 3e.video-ak.cdn.spotify.com","0.0.0.0 3f.video-ak.cdn.spotify.com","0.0.0.0 40.video-ak.cdn.spotify.com","0.0.0.0 41.video-ak.cdn.spotify.com","0.0.0.0 42.video-ak.cdn.spotify.com","0.0.0.0 43.video-ak.cdn.spotify.com","0.0.0.0 44.video-ak.cdn.spotify.com","0.0.0.0 45.video-ak.cdn.spotify.com","0.0.0.0 46.video-ak.cdn.spotify.com","0.0.0.0 47.video-ak.cdn.spotify.com","0.0.0.0 48.video-ak.cdn.spotify.com","0.0.0.0 49.video-ak.cdn.spotify.com","0.0.0.0 4a.video-ak.cdn.spotify.com","0.0.0.0 4b.video-ak.cdn.spotify.com","0.0.0.0 4c.video-ak.cdn.spotify.com","0.0.0.0 4d.video-ak.cdn.spotify.com","0.0.0.0 4e.video-ak.cdn.spotify.com","0.0.0.0 4f.video-ak.cdn.spotify.com","0.0.0.0 50.video-ak.cdn.spotify.com","0.0.0.0 51.video-ak.cdn.spotify.com","0.0.0.0 52.video-ak.cdn.spotify.com","0.0.0.0 53.video-ak.cdn.spotify.com","0.0.0.0 54.video-ak.cdn.spotify.com","0.0.0.0 55.video-ak.cdn.spotify.com","0.0.0.0 56.video-ak.cdn.spotify.com","0.0.0.0 64.video-ak.cdn.spotify.com","0.0.0.0 6d.video-ak.cdn.spotify.com","0.0.0.0 6e.video-ak.cdn.spotify.com","0.0.0.0 6f.video-ak.cdn.spotify.com","0.0.0.0 70.video-ak.cdn.spotify.com","0.0.0.0 71.video-ak.cdn.spotify.com","0.0.0.0 72.video-ak.cdn.spotify.com","0.0.0.0 73.video-ak.cdn.spotify.com","0.0.0.0 74.video-ak.cdn.spotify.com","0.0.0.0 75.video-ak.cdn.spotify.com","0.0.0.0 76.video-ak.cdn.spotify.com","0.0.0.0 77.video-ak.cdn.spotify.com","0.0.0.0 78.video-ak.cdn.spotify.com","0.0.0.0 79.video-ak.cdn.spotify.com","0.0.0.0 7a.video-ak.cdn.spotify.com","0.0.0.0 7b.video-ak.cdn.spotify.com","0.0.0.0 7c.video-ak.cdn.spotify.com","0.0.0.0 7d.video-ak.cdn.spotify.com","0.0.0.0 7e.video-ak.cdn.spotify.com","0.0.0.0 7f.video-ak.cdn.spotify.com","0.0.0.0 80.video-ak.cdn.spotify.com","0.0.0.0 81.video-ak.cdn.spotify.com","0.0.0.0 82.video-ak.cdn.spotify.com","0.0.0.0 83.video-ak.cdn.spotify.com","0.0.0.0 84.video-ak.cdn.spotify.com","0.0.0.0 85.video-ak.cdn.spotify.com","0.0.0.0 86.video-ak.cdn.spotify.com","0.0.0.0 87.video-ak.cdn.spotify.com","0.0.0.0 88.video-ak.cdn.spotify.com","0.0.0.0 89.video-ak.cdn.spotify.com","0.0.0.0 8a.video-ak.cdn.spotify.com","0.0.0.0 8b.video-ak.cdn.spotify.com","0.0.0.0 8c.video-ak.cdn.spotify.com","0.0.0.0 8d.video-ak.cdn.spotify.com","0.0.0.0 8e.video-ak.cdn.spotify.com","0.0.0.0 8f.video-ak.cdn.spotify.com","0.0.0.0 90.video-ak.cdn.spotify.com","0.0.0.0 91.video-ak.cdn.spotify.com","0.0.0.0 92.video-ak.cdn.spotify.com","0.0.0.0 93.video-ak.cdn.spotify.com","0.0.0.0 94.video-ak.cdn.spotify.com","0.0.0.0 95.video-ak.cdn.spotify.com","0.0.0.0 96.video-ak.cdn.spotify.com","0.0.0.0 97.video-ak.cdn.spotify.com","0.0.0.0 98.video-ak.cdn.spotify.com","0.0.0.0 99.video-ak.cdn.spotify.com","0.0.0.0 9a.video-ak.cdn.spotify.com","0.0.0.0 9b.video-ak.cdn.spotify.com","0.0.0.0 9c.video-ak.cdn.spotify.com","0.0.0.0 9d.video-ak.cdn.spotify.com","0.0.0.0 9e.video-ak.cdn.spotify.com","0.0.0.0 9f.video-ak.cdn.spotify.com","0.0.0.0 a0.video-ak.cdn.spotify.com","0.0.0.0 a1.video-ak.cdn.spotify.com","0.0.0.0 a2.video-ak.cdn.spotify.com","0.0.0.0 a3.video-ak.cdn.spotify.com","0.0.0.0 a4.video-ak.cdn.spotify.com","0.0.0.0 a5.video-ak.cdn.spotify.com","0.0.0.0 a6.video-ak.cdn.spotify.com","0.0.0.0 a7.video-ak.cdn.spotify.com","0.0.0.0 a8.video-ak.cdn.spotify.com","0.0.0.0 a9.video-ak.cdn.spotify.com","0.0.0.0 aa.video-ak.cdn.spotify.com","0.0.0.0 ab.video-ak.cdn.spotify.com","0.0.0.0 ac.video-ak.cdn.spotify.com","0.0.0.0 ad.video-ak.cdn.spotify.com","0.0.0.0 ae.video-ak.cdn.spotify.com","0.0.0.0 af.video-ak.cdn.spotify.com","0.0.0.0 b0.video-ak.cdn.spotify.com","0.0.0.0 b1.video-ak.cdn.spotify.com","0.0.0.0 b2.video-ak.cdn.spotify.com","0.0.0.0 b3.video-ak.cdn.spotify.com","0.0.0.0 b4.video-ak.cdn.spotify.com","0.0.0.0 b5.video-ak.cdn.spotify.com","0.0.0.0 b6.video-ak.cdn.spotify.com","0.0.0.0 b7.video-ak.cdn.spotify.com","0.0.0.0 b8.video-ak.cdn.spotify.com","0.0.0.0 b9.video-ak.cdn.spotify.com","0.0.0.0 ba.video-ak.cdn.spotify.com","0.0.0.0 bb.video-ak.cdn.spotify.com","0.0.0.0 bc.video-ak.cdn.spotify.com","0.0.0.0 bd.video-ak.cdn.spotify.com","0.0.0.0 be.video-ak.cdn.spotify.com","0.0.0.0 bf.video-ak.cdn.spotify.com","0.0.0.0 c0.video-ak.cdn.spotify.com","0.0.0.0 c1.video-ak.cdn.spotify.com","0.0.0.0 c2.video-ak.cdn.spotify.com","0.0.0.0 c3.video-ak.cdn.spotify.com","0.0.0.0 c4.video-ak.cdn.spotify.com","0.0.0.0 c5.video-ak.cdn.spotify.com","0.0.0.0 c6.video-ak.cdn.spotify.com","0.0.0.0 c7.video-ak.cdn.spotify.com","0.0.0.0 c8.video-ak.cdn.spotify.com","0.0.0.0 c9.video-ak.cdn.spotify.com","0.0.0.0 ca.video-ak.cdn.spotify.com","0.0.0.0 cb.video-ak.cdn.spotify.com","0.0.0.0 cc.video-ak.cdn.spotify.com","0.0.0.0 cd.video-ak.cdn.spotify.com","0.0.0.0 ce.video-ak.cdn.spotify.com","0.0.0.0 cf.video-ak.cdn.spotify.com","0.0.0.0 d0.video-ak.cdn.spotify.com","0.0.0.0 d1.video-ak.cdn.spotify.com","0.0.0.0 d2.video-ak.cdn.spotify.com","0.0.0.0 d3.video-ak.cdn.spotify.com","0.0.0.0 d4.video-ak.cdn.spotify.com","0.0.0.0 d5.video-ak.cdn.spotify.com","0.0.0.0 d6.video-ak.cdn.spotify.com","0.0.0.0 d7.video-ak.cdn.spotify.com","0.0.0.0 d8.video-ak.cdn.spotify.com","0.0.0.0 d9.video-ak.cdn.spotify.com","0.0.0.0 da.video-ak.cdn.spotify.com","0.0.0.0 db.video-ak.cdn.spotify.com","0.0.0.0 dc.video-ak.cdn.spotify.com","0.0.0.0 dd.video-ak.cdn.spotify.com","0.0.0.0 de.video-ak.cdn.spotify.com","0.0.0.0 df.video-ak.cdn.spotify.com","0.0.0.0 e0.video-ak.cdn.spotify.com","0.0.0.0 e1.video-ak.cdn.spotify.com","0.0.0.0 e2.video-ak.cdn.spotify.com","0.0.0.0 e3.video-ak.cdn.spotify.com","0.0.0.0 e4.video-ak.cdn.spotify.com","0.0.0.0 e5.video-ak.cdn.spotify.com","0.0.0.0 e6.video-ak.cdn.spotify.com","0.0.0.0 e7.video-ak.cdn.spotify.com","0.0.0.0 e8.video-ak.cdn.spotify.com","0.0.0.0 e9.video-ak.cdn.spotify.com","0.0.0.0 ea.video-ak.cdn.spotify.com","0.0.0.0 eb.video-ak.cdn.spotify.com","0.0.0.0 ec.video-ak.cdn.spotify.com","0.0.0.0 ed.video-ak.cdn.spotify.com","0.0.0.0 ee.video-ak.cdn.spotify.com","0.0.0.0 ef.video-ak.cdn.spotify.com","0.0.0.0 f0.video-ak.cdn.spotify.com","0.0.0.0 f1.video-ak.cdn.spotify.com","0.0.0.0 f2.video-ak.cdn.spotify.com","0.0.0.0 f3.video-ak.cdn.spotify.com","0.0.0.0 f4.video-ak.cdn.spotify.com","0.0.0.0 f5.video-ak.cdn.spotify.com","0.0.0.0 f6.video-ak.cdn.spotify.com","0.0.0.0 f7.video-ak.cdn.spotify.com","0.0.0.0 f8.video-ak.cdn.spotify.com","0.0.0.0 f9.video-ak.cdn.spotify.com","0.0.0.0 fc.video-ak.cdn.spotify.com","0.0.0.0 fd.video-ak.cdn.spotify.com","0.0.0.0 fe.video-ak.cdn.spotify.com","0.0.0.0 ff.video-ak.cdn.spotify.com","0.0.0.0 beta.spotify.map.fastly.net","0.0.0.0 beta.spotify.map.fastlylb.net","0.0.0.0 prod.spotify-heads.map.fastlylb.net","0.0.0.0 prod.spotify.map.fastly.net","0.0.0.0 prod.spotify.map.fastlylb.net","0.0.0.0 ipinfo.io","0.0.0.0 cdn.ravenjs.com","0.0.0.0 links.iterable.com","0.0.0.0 adjust.com","0.0.0.0 atom.adjust.com","0.0.0.0 events.adjust.com","0.0.0.0 ulink.adjust.com","0.0.0.0 view.adjust.com","0.0.0.0 a.admob.com","0.0.0.0 ac1.vip.sc9.admob.com","0.0.0.0 ac208.sc9.admob.com","0.0.0.0 admob.biz","0.0.0.0 admob.co.kr","0.0.0.0 admob.co.nz","0.0.0.0 admob.co.uk","0.0.0.0 admob.com","0.0.0.0 admob.de","0.0.0.0 admob.dk","0.0.0.0 admob.es","0.0.0.0 admob.fi","0.0.0.0 admob.fr","0.0.0.0 admob.gr","0.0.0.0 admob.it","0.0.0.0 admob.jp","0.0.0.0 admob.kr","0.0.0.0 admob.mobi","0.0.0.0 admob.no","0.0.0.0 admob.pt","0.0.0.0 admob.sg","0.0.0.0 admob.tk","0.0.0.0 admob.tw","0.0.0.0 ads.admob.com","0.0.0.0 analytics.admob.com","0.0.0.0 api.admob.com","0.0.0.0 api.admob.xiaomi.com","0.0.0.0 b.admob.com","0.0.0.0 broadcast.admob.com","0.0.0.0 c.admob.com","0.0.0.0 c1.vip.sc9.admob.com","0.0.0.0 clk1.vip.sc9.admob.com","0.0.0.0 clk2.vip.sc9.admob.com","0.0.0.0 clk3.vip.sc9.admob.com","0.0.0.0 cpm.admob.com","0.0.0.0 cpm1.admob.com","0.0.0.0 cpm1.vip.sc9.admob.com","0.0.0.0 cpm2.admob.com","0.0.0.0 cpm2.vip.sc9.admob.com","0.0.0.0 cpm3.admob.com","0.0.0.0 cpm3.vip.sc9.admob.com","0.0.0.0 d.admob.com","0.0.0.0 dep.admob.com","0.0.0.0 dev1.vip.sc9.admob.com","0.0.0.0 developer.admob.com","0.0.0.0 e.admob.com","0.0.0.0 eng.admob.com","0.0.0.0 f.admob.com","0.0.0.0 g.admob.com","0.0.0.0 games.admob.com","0.0.0.0 ge-0-0-1-edge1.sc9.admob.com","0.0.0.0 ge-0-0-43-crs1.sc9.admob.com","0.0.0.0 gw.admob.com","0.0.0.0 h.admob.com","0.0.0.0 i.admob.com","0.0.0.0 j.admob.com","0.0.0.0 jp.admob.com","0.0.0.0 k.admob.com","0.0.0.0 l.admob.com","0.0.0.0 m.admob.com","0.0.0.0 media.admob.com","0.0.0.0 metrics.admob.com","0.0.0.0 mm.admob.com","0.0.0.0 mm1.vip.sc1.admob.com","0.0.0.0 mm1.vip.sc9.admob.com","0.0.0.0 mm104.sc9.admob.com","0.0.0.0 mmv.admob.com","0.0.0.0 n.admob.com","0.0.0.0 network.admob.com","0.0.0.0 o.admob.com","0.0.0.0 p.admob.com","0.0.0.0 p1.vip.sc9.admob.com","0.0.0.0 paix1.sc1.admob.com","0.0.0.0 pixel.admobclick.com","0.0.0.0 pt.admob.com","0.0.0.0 q.admob.com","0.0.0.0 r.admob.com","0.0.0.0 r2.vip.sc9.admob.com","0.0.0.0 rmd.admob.com","0.0.0.0 s.admob.com","0.0.0.0 sc1.admob.com","0.0.0.0 sc9.admob.com","0.0.0.0 smtp.admob.com","0.0.0.0 smtp2.admob.com","0.0.0.0 t.admob.com","0.0.0.0 tap1.vip.sc9.admob.com","0.0.0.0 tic1.vip.sc9.admob.com","0.0.0.0 tracking.admobsphere.com","0.0.0.0 tx102.sc9.admob.com","0.0.0.0 tx103.sc9.admob.com","0.0.0.0 u.admob.com","0.0.0.0 ukr.admob.com","0.0.0.0 v.admob.com","0.0.0.0 vip.sc9.admob.com","0.0.0.0 w.admob.com","0.0.0.0 wap.admob.com","0.0.0.0 waps.admob.com","0.0.0.0 wendy.admob.com","0.0.0.0 www.admob.com","0.0.0.0 www1.vip.sc9.admob.com","0.0.0.0 x.admob.com","0.0.0.0 y.admob.com","0.0.0.0 z.admob.com","0.0.0.0 zhcn.admob.com","0.0.0.0 a1294.w20.akamai.net","0.0.0.0 a1843.g.akamai.net","0.0.0.0 a50.g2.akamai.net","0.0.0.0 bsams.eyeblaster.akadns.net","0.0.0.0 e7876.dscg.akamaiedge.net","0.0.0.0 e8218.dscb1.akamaiedge.net","0.0.0.0 na.gmtdmp.com","0.0.0.0 d2gi7ultltnc2u.cloudfront.net","0.0.0.0 lb1-217593028.us-east-1.elb.amazonaws.com","0.0.0.0 rp-lb-766892831.us-east-1.elb.amazonaws.com","0.0.0.0 server-54-230-216-203.mrs50.r.cloudfront.net","0.0.0.0 adserver.adtechus.com","0.0.0.0 a.adk2x.com","0.0.0.0 adnxs.com","0.0.0.0 ams1-ib.adnxs.com","0.0.0.0 ib.adnxs.com","0.0.0.0 live.adyen.com","0.0.0.0 bounceexchange.com","0.0.0.0 admetrix.comscore.com","0.0.0.0 api.comscore.com","0.0.0.0 aser.comscore.com","0.0.0.0 cdr.comscore.com","0.0.0.0 cer.comscore.com","0.0.0.0 comscore.net","0.0.0.0 comscore.org","0.0.0.0 comscoredatagems.com","0.0.0.0 comscoredatamine.com","0.0.0.0 comscoredirect.net","0.0.0.0 comscorenetworks.net","0.0.0.0 ia1.comscore.com","0.0.0.0 ia2.comscore.com","0.0.0.0 iad.comscore.com","0.0.0.0 im.comscore.com","0.0.0.0 images.comscore.com","0.0.0.0 labs.comscore.com","0.0.0.0 mail2.comscore.com","0.0.0.0 marketer.comscore.com","0.0.0.0 mx0.comscore.com","0.0.0.0 mx1.comscore.com","0.0.0.0 mx2.comscore.com","0.0.0.0 my.comscore.com","0.0.0.0 mycomscore.com","0.0.0.0 mymetrix.comscore.com","0.0.0.0 myresearch.comscore.com","0.0.0.0 ord.comscore.com","0.0.0.0 postbuy.comscore.com","0.0.0.0 proclarity.comscore.com","0.0.0.0 qat.comscore.com","0.0.0.0 siterecruit.comscore.com","0.0.0.0 svn.comscore.com","0.0.0.0 www.comscore.com","0.0.0.0 www.mycomscore.net","0.0.0.0 crashlytics.163.com","0.0.0.0 crashlytics.com","0.0.0.0 crashlytics.twimg.com","0.0.0.0 e.crashlytics.com","0.0.0.0 reports.crashlytics.com","0.0.0.0 settings-crashlytics-1410998606.us-east-1.elb.amazonaws.com","0.0.0.0 settings-crashlytics-b-103974621.us-east-1.elb.amazonaws.com","0.0.0.0 settings.crashlytics.com","0.0.0.0 combine.urbanairship.com","0.0.0.0 ads-west-colo.adsymptotic.com","0.0.0.0 gtssl2-ocsp.geotrust.com","0.0.0.0 ad.doubleclick.net","0.0.0.0 adclick.g.doublecklick.net","0.0.0.0 ade.googlesyndication.com","0.0.0.0 adservice.google.com","0.0.0.0 analytics.google.com","0.0.0.0 doubleclick.net","0.0.0.0 ee-in-f95.1.e100.net","0.0.0.0 gcdn.2mdn.net","0.0.0.0 googleads4.g.doubleclick.net","0.0.0.0 googleadservices.com","0.0.0.0 pagead.l.doubleclick.net","0.0.0.0 pagead-googlehosted.l.google.com","0.0.0.0 pagead46.l.doubleclick.net","0.0.0.0 partner.googleadservices.com","0.0.0.0 partnerad.l.doubleclick.net","0.0.0.0 s0-2mdn-net.l.google.com","0.0.0.0 video-ad-stats.googlesyndication.com","0.0.0.0 www.googleadservices.com","0.0.0.0 anycast.pixel.adsafeprotected.com","0.0.0.0 api-iam.intercom.io","0.0.0.0 api-ping.intercom.io","0.0.0.0 api.intercom.io","0.0.0.0 developers.intercom.io","0.0.0.0 engineering.intercom.io","0.0.0.0 funky.staging.intercom.io","0.0.0.0 intercom.io","0.0.0.0 internal.intercom.io","0.0.0.0 leadvisualdesigner.intercom.io","0.0.0.0 mobile-sdk-api.intercom.io","0.0.0.0 muster-sandbox.intercom.io","0.0.0.0 nexus-testing.intercom.io","0.0.0.0 nexus-websocket-a.intercom.io","0.0.0.0 nexus.intercom.io","0.0.0.0 orientation.internal.intercom.io","0.0.0.0 status.intercom.io","0.0.0.0 store.intercom.io","0.0.0.0 terraform-modules-test.intercom.io","0.0.0.0 via.intercom.io","0.0.0.0 w.intercom.io","0.0.0.0 widget.intercom.io","0.0.0.0 woody.intercom.io","0.0.0.0 www.intercom.io","0.0.0.0 core.insightexpressai.com","0.0.0.0 track.leanlab.co","0.0.0.0 content.bitsontherun.com","0.0.0.0 v.jwpcdn.com","0.0.0.0 eu-gmtdmp.gd1.mookie1.com","0.0.0.0 media-match.com","0.0.0.0 ab.tune.com","0.0.0.0 academy.tune.com","0.0.0.0 beta.tune.com","0.0.0.0 click.api.cp.tune.com","0.0.0.0 configuration.ma.tune.com","0.0.0.0 corp.tune.com","0.0.0.0 cp.tune.com","0.0.0.0 dev.lab.tune.com","0.0.0.0 event.api.cp.tune.com","0.0.0.0 lab.tune.com","0.0.0.0 labs.tune.com","0.0.0.0 ma.tune.com","0.0.0.0 mkt.tune.com","0.0.0.0 multiverse.tune.com","0.0.0.0 ops-prod.us-west-2.k8s.ops.tune.com","0.0.0.0 playlist.ma.tune.com","0.0.0.0 request.api.cp.tune.com","0.0.0.0 stage.tune.com","0.0.0.0 support.tune.com","0.0.0.0 tune.com","0.0.0.0 ui.cp.tune.com","0.0.0.0 updates.tune.com","0.0.0.0 www.cp.tune.com","0.0.0.0 www.ui.cp.tune.com","0.0.0.0 api.mixpanel.com","0.0.0.0 decide.mixpanel.com","0.0.0.0 mixpanel.com","0.0.0.0 switchboard.mixpanel.com","0.0.0.0 www.mixpanel.com","0.0.0.0 adgrx.moatads.com","0.0.0.0 afs.moatads.com","0.0.0.0 apx.moatads.com","0.0.0.0 bd.moatads.com","0.0.0.0 c-evt.moatads.com","0.0.0.0 dbg52463.moatads.com","0.0.0.0 ejs.moatads.com","0.0.0.0 evt.moatads.com","0.0.0.0 fs.moatads.com","0.0.0.0 geo.moatads.com","0.0.0.0 jm.moatads.com","0.0.0.0 js.moatads.com","0.0.0.0 json.moatads.com","0.0.0.0 jsonp.moatads.com","0.0.0.0 logtest.moatads.com","0.0.0.0 moat.pxl.ace.advertising.com","0.0.0.0 moatads.com","0.0.0.0 pixel.moatads.com","0.0.0.0 pool3.moatads.com","0.0.0.0 pool5.moatads.com","0.0.0.0 post.update.moatads.com","0.0.0.0 px.moatads.com","0.0.0.0 quantcast584928381.s.moatpixel.com","0.0.0.0 r.254a.comjs.moatads.com","0.0.0.0 s-jsonp.moatads.com","0.0.0.0 s.moatads.com","0.0.0.0 s.update.moatads.com","0.0.0.0 sejs.moatads.com","0.0.0.0 svast.moatads.com","0.0.0.0 svastx.moatads.com","0.0.0.0 t.update.moatads.com","0.0.0.0 tribpubdfp745347008913.s.moatpixel.com","0.0.0.0 u.moatads.com","0.0.0.0 update.moatads.com","0.0.0.0 upv4.moatads.com","0.0.0.0 v3.moatads.com","0.0.0.0 v4.moatads.com","0.0.0.0 vast.moatads.com","0.0.0.0 vastx.moatads.com","0.0.0.0 vpb0.moatads.com","0.0.0.0 vu.moatads.com","0.0.0.0 wildcard.moatads.com.edgekey.net","0.0.0.0 www.moatads.com","0.0.0.0 www.upv4.moatads.com","0.0.0.0 y.moatads.com","0.0.0.0 yj.moatads.com","0.0.0.0 yt.moatads.com","0.0.0.0 yts.moatads.com","0.0.0.0 z.moatads.com","0.0.0.0 t.myvisualiq.net","0.0.0.0 vt.myvisualiq.net","0.0.0.0 omaze.com","0.0.0.0 www.omaze.com","0.0.0.0 gads22000.pubmatic.com","0.0.0.0 showads33000.pubmatic.com","0.0.0.0 b.scorecardresearch.com","0.0.0.0 sb.scorecardresearch.com","0.0.0.0 udm.scorecardresearch.com","0.0.0.0 bs.serving-sys.com","0.0.0.0 ds.serving-sys.com","0.0.0.0 cs126.wpc.teliasoneracdn.net","0.0.0.0 cs283.wpc.teliasoneracdn.net","0.0.0.0 seen-on-screen.thewhizmarketing.com","0.0.0.0 cs126.wpc.edgecastcdn.net","0.0.0.0 cs934.wac.thetacdn.net","0.0.0.0 ar.voicefive.com","0.0.0.0 sb.voicefive.com","0.0.0.0 media-router-flurry7.prod.media.wg1.b.yahoo.com","0.0.0.0 api.zwizzarmyknife.com","0.0.0.0 163-172-209-161.rev.poneytelecom.eu","0.0.0.0 mil04s27-in-f131.1e100.net","0.0.0.0 mil04s27-in-f142.1e100.net","0.0.0.0 ec2-52-7-112-21.compute-1.amazonaws.com","0.0.0.0 20.65.199.104.bc.googleusercontent.com","0.0.0.0 144.64.199.104.bc.googleusercontent.com","0.0.0.0 speech-recognition-test.spotify.com","0.0.0.0 speech-recognition.spotify.com","0.0.0.0 prg03s02-in-f110.1e100.net","0.0.0.0 prg03s02-in-f99.1e100.net","0.0.0.0 qnajplxtvz-a.akamaihd.net","0.0.0.0 austin.logs.roku.com","0.0.0.0 ec2-52-202-9-181.compute-1.amazonaws.com","0.0.0.0 ec2-54-88-142-76.compute-1.amazonaws.com","0.0.0.0 a2-19-66-11.deploy.static.akamaitechnologies.com","0.0.0.0 a3-19-66-11.deploy.static.akamaitechnologies.com","0.0.0.0 a23-54-115-202.deploy.static.akamaitechnologies.com","0.0.0.0 ec2-54-88-58-219.compute-1.amazonaws.com","0.0.0.0 ec2-34-195-40-20.compute-1.amazonaws.com","0.0.0.0 accalia.ap.spotify.com","0.0.0.0 accalia.ash.spotify.com","0.0.0.0 accalia.ash1.spotify.com","0.0.0.0 adjust-callback.spotify.com","0.0.0.0 adobe-dns-2.spotify.com","0.0.0.0 afton.ap.spotify.com","0.0.0.0 agnes.ap.spotify.com","0.0.0.0 ahava.ap.spotify.com","0.0.0.0 smartling-001.lon.spotify.com","0.0.0.0 smartling-001.lon2.spotify.com","0.0.0.0 surveys.spotify.com","0.0.0.0 sap.spotify.com","0.0.0.0 sap-testing.spotify.com","0.0.0.0 valerie.ap.spotify.com","0.0.0.0 valerie.lon.spotify.com","0.0.0.0 valerie.lon2.spotify.com","0.0.0.0 sto3-tbdscan-a2.sto3.spotify.com","0.0.0.0 sto3-vmddelivery-b1.sto3.spotify.com","0.0.0.0 sto1-apresolve-a3.sto.spotify.com","0.0.0.0 sto1-crashdump-a1.sto.spotify.com","0.0.0.0 sto1-crashdump-a2.sto.spotify.com","0.0.0.0 skye.ap.spotify.com","0.0.0.0 skye.sto.spotify.com","0.0.0.0 shulamit.ash1.spotify.com","0.0.0.0 sienna.ap.spotify.com","0.0.0.0 sienna.sto.spotify.com","0.0.0.0 sigrid.lon.spotify.com","0.0.0.0 sirkka.lon.spotify.com","0.0.0.0 sirkka.lon2.spotify.com","0.0.0.0 r620test.lon.spotify.com","0.0.0.0 pub-net-srx-int-testing.spotify.com","0.0.0.0 promo-001.lon.spotify.com","0.0.0.0 promo.spotify.com","0.0.0.0 pay-testing.spotify.com","0.0.0.0 oktascimpoc.spotify.com","0.0.0.0 oktascimserver-test.spotify.com","0.0.0.0 oktascimserver.spotify.com","0.0.0.0 kismet.ap.spotify.com","0.0.0.0 heather.ap.spotify.com","0.0.0.0 guc-dealer-ssl.spotify.com","0.0.0.0 guc-dealer.spotify.com","0.0.0.0 devnews.spotify.com","0.0.0.0 beta.spotify.com","0.0.0.0 beta.developer.spotify.com","0.0.0.0 adsgtm-sandbox.spotify.com","0.0.0.0 adsgtm.spotify.com","0.0.0.0 48.65.199.104.bc.googleusercontent.com","0.0.0.0 www.contest.codequest.spotify.com","0.0.0.0 www.codequest.spotify.com","0.0.0.0 exp-canary.wg.spotify.com","0.0.0.0 exp.spotify.com","0.0.0.0 exp.wg.spotify.com","0.0.0.0 klara.lon.spotify.com","0.0.0.0 investors.spotify.com","0.0.0.0 indosatooredoo.spotify.com","0.0.0.0 imqvs2qippm65n6g2qtyyqimyeggervnt64cyzl2x2fduo6g.tb6aoen5rgxsd3vrsm5x6257pe43ez5jpbpqs7mnhfd2gwpm.6p43cx5jyukului62krjkjbuw27drk43s2wizh2rqbzqnoix.rr4lyf2ny4j5pit663giazaneive3wnxoi67oietaezfarxq.nvkux5go6wfzevmi.er.spo.spotify.com","0.0.0.0 heads-l3.spotify.com","0.0.0.0 zayit.ap.spotify.com","0.0.0.0 zayit.lon.spotify.com","0.0.0.0 zayit.lon2.spotify.com","0.0.0.0 xfactor.spotify.com","0.0.0.0 wwwtools.spotify.com","0.0.0.0 www.www.spotify.com","0.0.0.0 vpn-ash1-1.ash.spotify.com","0.0.0.0 vpn-lon-001.lon.spotify.com","0.0.0.0 upsell.scdn.co","0.0.0.0 sgwww.spotify.com","0.0.0.0 shavar.developer-services.mozilla.com","0.0.0.0 shavar.services.mozilla.com","0.0.0.0 quicksilver.scdn.co","0.0.0.0 pixel-static.spotify.com","0.0.0.0 suk6mvixsl7e7pnqmcxzq2nlrv23orxa4lnmqehevie5lr46.qcfzwrpafytxzuzudly53cdpuinbus5dryc5yyluqfgstdbk.ifcouvs4yrkvq54g3sqlvxg6iwtieqko3d4z6g43c2na2xok.5l6armklu65sqea3msfquneq7rjwynp42nva5f7pl5qx4a7x.erz4onrdnlviivki.er.spo.spotify.com","0.0.0.0 qm2adg47liahr7nsp3hu3do3v4rhjzv7o5z2e4gffuri4fzk.22aradra3s5midyr2lud47ipbxupxt52kzcmfsblo7zjty7a.sx2gdu6lcitzksm3d2zgxbxg5ppfc7mp7uanbvwjxq7wac2i.4doohcbyhkrp2eb4d7gsqareg6h5h73uespoxkefxgf23dua.dl6tyv4jcseeishe.er.spo.spotify.com","0.0.0.0 pl35t7bb4aabglvz2i3btceznecjh4fka7m6ppyzdthifgcq.obfxt53kpvuicjyib3vacc5db3htskgk62k5tsonipsj2pty.yoyjueb2cieypg5vnrb5jpkuf52xxafpqf4ecynqzuz6s42t.tiox3ktdzfwwbcokrxi6o7huoya6iogdfo7ktcsdhkxakyjw.sg7cexr7oxb2d6ms.er.spo.spotify.com","0.0.0.0 pl35t7bb4aabglvz2i3btceznecjh4fka7m6ppyzdthifgcq.obfxt53kpvuicjyib3vacc5db3htskgk62k5tsonipsj2pty.yoyjueb2cieypg5vnrb5jpkuf52xxafpqf4ecynqzuz6s42t.tiox3ktdzfwwbcokrxi6o7huoya6iogdfo7ktcsdhkxakyjw.sg7cexr7o7b2d6ms.er.spo.spotify.com","0.0.0.0 gs5j7zyhogb53dmqmjfqljsq2ykksm3.spotify.com","0.0.0.0 bug.spotify.com","0.0.0.0 labels-canary.spotify.com","0.0.0.0 bundling-integration.spotify.com","0.0.0.0 analytics-001.lon.spotify.com","0.0.0.0 analytics-002.lon.spotify.com","0.0.0.0 analytics-classic.spotify.com","0.0.0.0 analytics-preview.spotify.com","0.0.0.0 adstudio-help.spotify.com","0.0.0.0 adsgtm-dev.spotify.com","0.0.0.0 ads-fa.spotify.com","0.0.0.0 admocker.spotify.com","0.0.0.0 adlog.spotify.com","0.0.0.0 adgen.spotify.com","0.0.0.0 adgen-dev.spotify.com","0.0.0.0 ad.audio-ak.cdn.spotify.com","0.0.0.0 a1.audio-ak.cdn.spotify.com","0.0.0.0 ad-proxy.spotify.com","0.0.0.0 partner-provisioning.spotify.com","0.0.0.0 kismet.ash1.spotify.com","0.0.0.0 kismet.ash.spotify.com","0.0.0.0 28.64.199.104.bc.googleusercontent.com","0.0.0.0 zaira.lon.spotify.com","0.0.0.0 zaira.lon2.spotify.com","0.0.0.0 muc11s15-in-f3.1e100.net","0.0.0.0 fra15s17-in-f3.1e100.net","0.0.0.0 ec2-52-212-113-202.eu-west-1.compute.amazonaws.com","0.0.0.0 n1nlhg562c1562.shr.prod.ams1.secureserver.net","0.0.0.0 n2nlhg562c1562.shr.prod.ams1.secureserver.net","0.0.0.0 n2nlhg562c1562.shr.prod.ams2.secureserver.net","0.0.0.0 ec2-54-248-172-197.eu-west-1.compute.amazonaws.com","0.0.0.0 ec2-54-246-172-197.eu-west-1.compute.amazonaws.com","0.0.0.0 a23-45-105-248.deploy.static.akamaitechnologies.com","0.0.0.0 full-cdn-01.cluster002.ovh.net","0.0.0.0 full-cdn-01.cluster001.ovh.net","0.0.0.0 192.121.140.177","0.0.0.0 153.64.199.104.bc.googleusercontent.com","0.0.0.0 62.224.186.35.bc.googleusercontent.com","0.0.0.0 48.224.186.35.bc.googleusercontent.com","0.0.0.0 41.65.199.104.bc.googleusercontent.com","0.0.0.0 auditdesk-server.spotify.com","0.0.0.0 audio-ec.spotify.com","0.0.0.0 audio-fac.spotify.com","0.0.0.0 heads-ec.spotify.com","0.0.0.0 heads-ecp.spotify.com","0.0.0.0 heads-ecb.spotify.com","0.0.0.0 crashdump-001.lon.spotify.com","0.0.0.0 crashdump.sto.spotify.com","0.0.0.0 iria.ash1.spotify.com","0.0.0.0 guc-spclient.spotify.com","0.0.0.0 iria.ash.spotify.com","0.0.0.0 partnerportal.spotify.com","0.0.0.0 premium-code-admin.lon.spotify.com","0.0.0.0 pixel1.spotify.com","0.0.0.0 pixel2.spotify.com","0.0.0.0 video-fa.spotify.com","0.0.0.0 ae.audio-ak.cdn.spotify.com","0.0.0.0 af.audio-ak.cdn.spotify.com","0.0.0.0 adstudio.spotify.com","0.0.0.0 ffcampaign.spotify.com","0.0.0.0 buy-link-001.lon.spotify.com","0.0.0.0 bugzilla-001.lon.spotify.com","0.0.0.0 adlab.spotify.com","0.0.0.0 bloodhound.spotify.com","0.0.0.0 bf.audio-ak.cdn.spotify.com","0.0.0.0 bengta.lon.spotify.com","0.0.0.0 bengta.lon2.spotify.com","0.0.0.0 berdine.ash.spotify.com","0.0.0.0 berdine.ash1.spotify.com","0.0.0.0 ads-fa.cdn.spotify.com","0.0.0.0 ads-sp-ash.cdn.spotify.com","0.0.0.0 ads-sp-lon.cdn.spotify.com","0.0.0.0 ads-sp-sto.cdn.spotify.com","0.0.0.0 christopher.lon.spotify.com","0.0.0.0 beta.static.tune.com","127.0.0.1 media-match.com","127.0.0.1 adclick.g.doublecklick.net","127.0.0.1 www.googleadservices.com","127.0.0.1 open.spotify.com","127.0.0.1 pagead2.googlesyndication.com","127.0.0.1 desktop.spotify.com","127.0.0.1 googleads.g.doubleclick.net","127.0.0.1 pubads.g.doubleclick.net","127.0.0.1 securepubads.g.doubleclick.net","127.0.0.1 audio2.spotify.com","127.0.0.1 http://audio2.spotify.com","127.0.0.1 www.audio2.spotify.com","127.0.0.1 www.omaze.com","127.0.0.1 omaze.com","127.0.0.1 bounceexchange.com","127.0.0.1 core.insightexpressai.com","127.0.0.1 content.bitsontherun.com","127.0.0.1 s0.2mdn.net","127.0.0.1 v.jwpcdn.com","127.0.0.1 d2gi7ultltnc2u.cloudfront.net","127.0.0.1 crashdump.spotify.com","127.0.0.1 adeventtracker.spotify.com","127.0.0.1 log.spotify.com","127.0.0.1 analytics.spotify.com","127.0.0.1 ads-fa.spotify.com","127.0.0.1 cs283.wpc.teliasoneracdn.net","127.0.0.1 audio-ec.spotify.com","127.0.0.1 cs126.wpc.teliasoneracdn.net","127.0.0.1 heads-ec.spotify.com","127.0.0.1 u.scdn.co","127.0.0.1 cs126.wpc.edgecastcdn.net","127.0.0.1 pagead46.l.doubleclick.net","127.0.0.1 pagead.l.doubleclick.net","127.0.0.1 video-ad-stats.googlesyndication.com","127.0.0.1 pagead-googlehosted.l.google.com","127.0.0.1 partnerad.l.doubleclick.net","127.0.0.1 prod.spotify.map.fastlylb.net","127.0.0.1 adserver.adtechus.com","127.0.0.1 na.gmtdmp.com","127.0.0.1 anycast.pixel.adsafeprotected.com","127.0.0.1 ads.pubmatic.com","127.0.0.1 idsync-ext.rlcdn.com","127.0.0.1 www.googletagservices.com","127.0.0.1 sto3.spotify.com","127.0.0.1 spclient.wg.spotify.com","127.0.0.1 d361oi6ppvq2ym.cloudfront.net","127.0.0.1 gads.pubmatic.com","127.0.0.1 ads-west-colo.adsymptotic.com","127.0.0.1 geo3.ggpht.com","127.0.0.1 showads33000.pubmatic.com","127.0.0.1 http://www.googleadservices.com","127.0.0.1 googlehosted.l.googleusercontent.com","","0.0.0.0 spclient.wg.spotify.com","","","0.0.0.0 open.spotify.com","0.0.0.0 desktop.spotify.com","","0.0.0.0 audio2.spotify.com","","","","","","0.0.0.0 googlehosted.l.googleusercontent.com","0.0.0.0 d361oi6ppvq2ym.cloudfront.net","0.0.0.0 geo3.ggpht.com","","0.0.0.0 redirector.gvt1.com","","0.0.0.0 weblb-wg.gslb.spotify.com"
   )
Write-Host "" | Out-File -Encoding ASCII -Append $hosts_file
ForEach($domain in $domains) {
    If (-Not (Select-String -Path $hosts_file -Pattern $domain)) {
    	Write-Host "0.0.0.0 $domain" | Out-File -Encoding ASCII -Append $hosts_file
    }
}
Write-Host "Adding telemetry ips to firewall"
$ips = @("134.170.30.202","137.116.81.24","157.56.106.189","184.86.53.99","2.22.61.43","2.22.61.66",
         "204.79.197.200","23.218.212.69","65.39.117.230","65.52.108.33","65.55.108.23","64.4.54.254")
Remove-NetFirewallRule -DisplayName "Block Telemetry IPs" -ErrorAction SilentlyContinue
New-NetFirewallRule -DisplayName "Block Telemetry IPs" -Direction Outbound -Action Block -RemoteAddress ([string[]]$ips)

#Disable Wifi Sense
Write-Host "Disabling Wifi-Sense..."
force-mkdir "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting"
Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -Name "Value" -Type DWord -Value 0
Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" -Name "Value" -Type DWord -Value 0

# Disable SmartScreen Filter
Write-Host "Disabling SmartScreen Filter..."
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\Explorer" -Name "SmartScreenEnabled" -Type String -Value "Off"
# Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AppHost" -Name "EnableWebContentEvaluation" -Type DWord -Value 0

# Disable Bing Search in Start Menu
Write-Host "Disabling Bing Search in Start Menu..."
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Search" -Name "BingSearchEnabled" -Type DWord -Value 0

# Disable Location Tracking
Write-Host "Disabling Location Tracking..."
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Sensor\Overrides\{BFA794E4-F964-4FDB-90F6-51056BFE4B44}" -Name "SensorPermissionState" -Type DWord -Value 0
Set-ItemProperty -Path "HKLM:\System\CurrentControlSet\Services\lfsvc\Service\Configuration" -Name "Status" -Type DWord -Value 0

# Disable Feedback
Write-Host "Disabling Feedback..."
force-mkdir "HKCU:\Software\Microsoft\Siuf\Rules"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Siuf\Rules" -Name "NumberOfSIUFInPeriod" -Type DWord -Value 0

# Disable Lock Screen
Write-Host "Disabling LockScreen..."
force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Personalization" -Name "NoLockScreen" -Type DWord -Value 1

# Disable Advertising ID
Write-Host "Disabling Advertising ID..."
force-mkdir "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -Name "Enabled" -Type DWord -Value 0

# Disable Cortana
Write-Host "Disabling Cortana..."
force-mkdir "HKCU:\Software\Microsoft\Personalization\Settings"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Personalization\Settings" -Name "AcceptedPrivacyPolicy" -Type DWord -Value 0
force-mkdir "HKCU:\Software\Microsoft\InputPersonalization"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\InputPersonalization" -Name "RestrictImplicitTextCollection" -Type DWord -Value 1
Set-ItemProperty -Path "HKCU:\Software\Microsoft\InputPersonalization" -Name "RestrictImplicitInkCollection" -Type DWord -Value 1
force-mkdir "HKCU:\Software\Microsoft\InputPersonalization\TrainedDataStore"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\InputPersonalization\TrainedDataStore" -Name "HarvestContacts" -Type DWord -Value 0

# Restrict Windows Update P2P only to local network
Write-Host "Restricting Windows Update P2P only to local network..."
Set-ItemProperty -Path "HKLM:\Software\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config" -Name "DODownloadMode" -Type DWord -Value 1
force-mkdir "HKCU:\Software\Microsoft\Windows\CurrentVersion\DeliveryOptimization"
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\DeliveryOptimization" -Name "SystemSettingsDownloadMode" -Type DWord -Value 3

# Remove AutoLogger file and restrict directory
Write-Host "Removing AutoLogger file and restricting directory..."
$autoLoggerDir = "$env:PROGRAMDATA\Microsoft\Diagnosis\ETLLogs\AutoLogger"
If (Test-Path "$autoLoggerDir\AutoLogger-Diagtrack-Listener.etl") {
    Remove-Item "$autoLoggerDir\AutoLogger-Diagtrack-Listener.etl"
}
icacls $autoLoggerDir /deny SYSTEM:`(OI`)`(CI`)F | Out-Null

# Stop and disable Diagnostics Tracking Service
Write-Host "Stopping and disabling Diagnostics Tracking Service..."
Stop-Service "DiagTrack"
Set-Service "DiagTrack" -StartupType Disabled

# Stop and disable WAP Push Service
Write-Host "Stopping and disabling WAP Push Service..."
Stop-Service "dmwappushservice"
Set-Service "dmwappushservice" -StartupType Disabled

#Stop and Disable Themes
Write-Host "Stopping and Disabling Themes..."
Stop-Service "Themes"
 Set-Service "Themes"-StartupType Disabled

#Stop and Disable Xbox Services
Write-Host "Stopping and disabling Xbox Services ..."
Stop-Service "XblAuthManager"
 Set-Service "XblAuthManager"-StartupType Disabled

Stop-Service "XblGameSave"
Set-Service "XblGameSave" -StartupType Disable

Stop-Service "XboxNetApiSvc"
Set-Service "XboxNetApiSvc" -StartupType Disable

# Disable Windows Defender
Write-Host "Disabling Windows Defender..."
Set-ItemProperty -Path "HKLM:\Software\Policies\Microsoft\Windows Defender" -Name "DisableAntiSpyware" -Type DWord -Value 1

# Stop and disable Home Groups services
Write-Host "Stopping and disabling Home Groups services..."
Stop-Service "HomeGroupListener"
Set-Service "HomeGroupListener" -StartupType Disabled
Stop-Service "HomeGroupProvider"
Set-Service "HomeGroupProvider" -StartupType Disabled

# Disable Action Center
Write-Host "Disabling Action Center..."
force-mkdir "HKCU:\Software\Policies\Microsoft\Windows\Explorer"
Set-ItemProperty -Path "HKCU:\Software\Policies\Microsoft\Windows\Explorer" -Name "DisableNotificationCenter" -Type DWord -Value 0

#Disable Sticky keys prompt
Write-Host "Disabling Sticky keys prompt..."
Set-ItemProperty -Path "HKCU:\Control Panel\Accessibility\StickyKeys" -Name "Flags" -Type String -Value "506"
Write-Host "Showing known file extensions..."
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "HideFileExt" -Type DWord -Value 0

# Show hidden files
Write-Host "Showing hidden files..."
Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" -Name "Hidden" -Type DWord -Value 1
# Disable OneDrive
Write-Host "Disabling OneDrive..."
force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive"
Set-ItemProperty -Path "HKLM:\SOFTWARE\Policies\Microsoft\Windows\OneDrive" -Name "DisableFileSyncNGSC" -Type DWord -Value 1

# Uninstall OneDrive
Stop-Service "OneSyncSvc"
Set-Service "OneSyncSvc" -StartupType Disable
Write-Host "Uninstalling OneDrive..."
Stop-Process -Name OneDrive -ErrorAction SilentlyContinue
Start-Sleep -s 3
$onedrive = "$env:SYSTEMROOT\SysWOW64\OneDriveSetup.exe"
If (!(Test-Path $onedrive)) {
	$onedrive = "$env:SYSTEMROOT\System32\OneDriveSetup.exe"
}
Start-Process $onedrive "/uninstall" -NoNewWindow -Wait
Start-Sleep -s 3
Stop-Process -Name explorer -ErrorAction SilentlyContinue
Start-Sleep -s 3
Remove-Item "$env:USERPROFILE\OneDrive" -Force -Recurse -ErrorAction SilentlyContinue
Remove-Item "$env:LOCALAPPDATA\Microsoft\OneDrive" -Force -Recurse -ErrorAction SilentlyContinue
Remove-Item "$env:PROGRAMDATA\Microsoft OneDrive" -Force -Recurse -ErrorAction SilentlyContinue
If (Test-Path "$env:SYSTEMDRIVE\OneDriveTemp") {
    Remove-Item "$env:SYSTEMDRIVE\OneDriveTemp" -Force -Recurse -ErrorAction SilentlyContinue
}
If (!(Test-Path "HKCR:")) {
    New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT | Out-Null
}
Remove-Item -Path "HKCR:\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -Recurse -ErrorAction SilentlyContinue
Remove-Item -Path "HKCR:\Wow6432Node\CLSID\{018D5C66-4533-4307-9B53-224DE2ED1FE6}" -Recurse -ErrorAction SilentlyContinue

# Uninstall Windows Features
Write-Host "Uninstalling default Microsoft applications..."
$featureList =('MicrosoftWindowsPowerShellV2Root','MicrosoftWindowsPowerShellV2','MediaPlayback','WindowsMediaPlayer',
                'WorkFolders-Client','SMB1Protocol','SMB1Protocol','SMB1Protocol-Deprecation','Windows-Defender-Default-Definitions',
                'Microsoft-Windows-NetFx3-OC-Package','Microsoft-Windows-NetFx3-WCF-OC-Package')
ForEach ($item in $featureList){
    dism /online /Disable-Feature /FeatureName:$item /quiet /norestart | Out-Null
}

#Uninstall Microsoft Apps and Store
ForEach($app in Get-AppxPackage | % {if (!($_.IsFramework -or $_.PackageFullName -ne "cw5n1h2txyewy")) {$_}}){
    If ($app -Contains "WindowsStore"){
        Remove-AppXProvisionedPackage -Online -PackageName $app
    }
    Remove-AppxPackage $app
}
Get-AppxPackage -AllUsers *store* | Remove-AppxPackage
# Try deleting the store completely
install_wim_tweak /o /c Microsoft-Windows-ContentDeliveryManager /r
install_wim_tweak /o /c Microsoft-Windows-Store /r
reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v RemoveWindowsStore /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\WindowsStore" /v DisableStoreApps /t REG_DWORD /d 1 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\AppHost" /v "EnableWebContentEvaluation" /t REG_DWORD /d 0 /f
reg add "HKLM\SOFTWARE\Policies\Microsoft\PushToInstall" /v DisablePushToInstall /t REG_DWORD /d 1 /f
reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\ContentDeliveryManager" /v SilentInstalledAppsEnabled /t REG_DWORD /d 0 /f
sc delete PushToInstall
sc delete InstallService

# Set Photo Viewer as default for bmp, gif, jpg and png
Write-Host "Setting Photo Viewer as default for bmp, gif, jpg, png and tif..."
If (!(Test-Path "HKCR:")) {
    New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT | Out-Null
}
ForEach ($type in @("Paint.Picture", "giffile", "jpegfile", "pngfile")) {
    New-Item -Path $("HKCR:\$type\shell\open") -Force | Out-Null
    New-Item -Path $("HKCR:\$type\shell\open\command") | Out-Null
    Set-ItemProperty -Path $("HKCR:\$type\shell\open") -Name "MuiVerb" -Type ExpandString -Value "@%ProgramFiles%\Windows Photo Viewer\photoviewer.dll,-3043"
    Set-ItemProperty -Path $("HKCR:\$type\shell\open\command") -Name "(Default)" -Type ExpandString -Value "%SystemRoot%\System32\rundll32.exe `"%ProgramFiles%\Windows Photo Viewer\PhotoViewer.dll`", ImageView_Fullscreen %1"
}

#Disable Store from Being reinstalled
Write-Host "Disabling Windows Cloud Content..."
force-mkdir "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content"
Set-ItemProperty "HKLM:\SOFTWARE\Policies\Microsoft\Windows\Cloud Content" "DisableWindowsConsumerFeatures" 1

#Install tools for full setup
If($installType -eq "Full"){
    Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    if($browser -eq "Chrome"){
        choco install googlechrome -y
    }
    else{
        choco install firefoxesr -y
    }
    choco install 7zip.install -y
    choco install sublimetext3 -y
    choco install pestudio -y
    choco install putty.install -y
    choco install vlc -y
    choco install classic-shell -y
    choco install malwarebytes -y --ignore-checksum
}

#Change the Update Settings
Write-Host "Changing Update Setting..."
$windowsUpdater = (New-Object -com "Microsoft.Update.AutoUpdate").Settings
$windowsUpdater.NotificationLevel=2 #Ask user before download and Install
$windowsUpdater.save()

# Disable windows Tips
reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableSoftLanding /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsSpotlightFeatures /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows\CloudContent" /v DisableWindowsConsumerFeatures /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\Windows\DataCollection" /v DoNotShowFeedbackNotifications /t REG_DWORD /d 1 /f
reg add "HKLM\Software\Policies\Microsoft\WindowsInkWorkspace" /v AllowSuggestedAppsInWindowsInkWorkspace /t REG_DWORD /d 0 /f

##########
# Restart
##########
Write-Host
Write-Host "Press any key to restart your system..." -ForegroundColor Black -BackgroundColor White
$key = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
Write-Host "Restarting..."
Restart-Computer
