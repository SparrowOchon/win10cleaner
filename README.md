# win10Cleaner
Windows 10 AIO Removal

##### The following will remove and disable any of the BS and bloat features in Windows 10 standard (Use LTSB if you can) . I am not responsible for damages that may occur or lack of productivity due to removal of needed features. Look throught the code and disable it for your setting.

###### Note this was made as an AIO solution as a result it uses code bases from both [Disassembler ](https://github.com/Disassembler0) and [W4RH4WK](https://github.com/W4RH4WK). It serves as a an automation of <del>[Fix10](https://fix10.isleaked.com/)</del> with additional fixes.

###### <del>For a well maintained alternative look at [BlackBird](https://www.getblackbird.net/)</del> Turns out Blackbird is very incomplete as such, I will continue updating this with newer patches.

# How to use?
  Simply download and execute the powershell script, note it will ask you for 2 parameters;
  * installType: The type of installation thats being performed I.e `Full` or `Min`
  * browser: The browser that will be installed I.e either `Chrome` or `Firefox` (only needed when Full installation is being performed can be set to `none`
  otherwise)


  ### Full Installation
This assumes this is a new system and will preinstall the following software;
* Firefox ESR or Chrome
* 7zip
* Sublime 3
* Putty
* VLC
* Classic Shell
* MalwareBytes
* Chocolatey
* PeStudio

### Min Installation
  This assumes the pc is already setup and will skip Installing additional software. NOTE: Windows Defender is disabled as part of this script. Thus install an antimalware.


  # What is changed during this Script:

  * Telemetry is Disabled
  * General Privacy Settings are changed to stop sending to MS
  * Syncing Settings with MS is disabled
  * Privacy Policy is changed
  * Disable MS Contact Scanning
  * Enable Security in Edge
  * Disable background access of default apps
  * Denying device access ( I.e all privacy settings via DeviceAccess subkeys)
  * [Apply MarkC's mouse acceleration fix](http://donewmouseaccel.blogspot.ca/2010/03/markc-windows-7-mouse-acceleration-fix.html)
  * Disable mouse pointer hiding
  * Restoring old volume slider
  * View File Extentions and Hidden Files
  * Setting default explorer view to This PC
  * Adding Telemetry and Spotify Ads to Redirect to 0.0.0.0 in the Hosts File
  * Adding telemetry ips to firewall
  * Disable Wifi Sense
  * Disable SmartScreen Filter
  * Disable Bing Search in Start Menu
  * Disable Location Tracking
  * Disable Feedback
  * Disable Advertising ID
  * Disable Cortana
  * Disable LockScreen
  * Disable Themes service
  * Disable Xbox Service
  * Restrict Windows Update P2P only to local network
  * Disable Animations
  * Remove AutoLogger file and restrict directory
  * Stop and disable Diagnostics Tracking Service
  * Stop and disable WAP Push Service
  * Disable Windows Defender
  * Stop and disable Home Groups services
  * Disable Action Center
  * Disable Sticky keys prompt
  * Disable and Uninstall OneDrive
  * Uninstall some Windows Features (Mediaplayer,SMBv1,NetFramework 3, Powershell v 2.0 )
  * Uninstall Microsoft Apps and Store (Note this will try to remove all apps installed using the Apps Store)
  * Set Photo Viewer as default for bmp, gif, jpg and png
  * Change the Update Settings (Ask the user when to update and what to install)





